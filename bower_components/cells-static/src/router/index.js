// constants
import { HOOKS } from './constants';
import CoreUtils from '../core/utils';

// core dependencies
import createRouter from 'router5';
import browserPlugin from 'router5/plugins/browser';

// router extensions
import middlewares from './middlewares/index';
import plugins from './plugins/index';

const { ROUTER, ROUTE } = HOOKS;
const { pageRender, progressivePageRender, routerLifeCycle } = plugins;
const { pageLoad, routeResolve } = middlewares;
const { pipePromises } = CoreUtils;

export default class Router {
  _instance = null;

  bootstrap(pageRenderConfig, routerConfig) {
    console.log('CellsStaticBridge() -> Router::bootstrap()');

    this._instance = this._initRouter(pageRenderConfig, routerConfig);
  }

  navigate(name, params, done) {
    this._instance.navigate(name, params, {}, done);
  }

  getCurrentRoute() {
    return this._instance.getState();
  }

  _initRouter(pageRenderConfig, routerConfig) {
    const { mainNode, componentPath, progressive, maxSimultaneousPages } = pageRenderConfig;
    const { useHash, routes, hooks, defaultRoute } = routerConfig;
    const options = {
      defaultRoute
    };

    const routesWithHooks = this._createRoutesWithHooks(routes, hooks);
    const router = createRouter(routesWithHooks, options);
    const dependencies = { routes };
    const pageRenderPlugin = progressive ? progressivePageRender(maxSimultaneousPages) : pageRender;

    router
      .setDependencies(dependencies)
      .usePlugin(browserPlugin({
        useHash: useHash
      }))
      .usePlugin(pageRenderPlugin(mainNode))
      .usePlugin(routerLifeCycle(hooks))
      .useMiddleware(pageLoad(componentPath))
      .useMiddleware(routeResolve(hooks))
      .start();

    return router;
  };

  _createRoutesWithHooks(routes, hooks) {
    const routerHooks = this._getRouterHooks(hooks);

    if (!routes) {
      return [];
    } else if (!routerHooks || routerHooks.length <= 0) {
      return routes;
    }

    return routes.map(this._mapRouteWithHooks.bind(null, routerHooks));
  }

  _getRouterHooks(hooks) {
    const HOOKS = [ROUTER.onTransitionActivate];
    const keys = Object.keys(hooks);

    if (keys.length <= 0) {
      return [];
    }

    return keys.filter(hook => HOOKS.includes(hook)).reduce((obj, key) => {
      obj.push(hooks[key]);
      return obj;
    }, []);
  }

  /**
   * Map given route object with specified route & router hooks (if exists).
   * We use Router5 canActivate hook handler to wrap our custom router and route
   * hooks so we can trigger them naturally on route lifecycle hook.
   *
   * @param  {Array}  hooks Array of router hooks to trigger wrap as route hook.
   * @param  {Object} route Route object.
   * @return {Object}       Processed Route object with wrapped hooks.
   */
  _mapRouteWithHooks(hooks, route) {
    const handlers = hooks;

    if (route.hasOwnProperty(ROUTE.onActivate)) {
      handlers.push(route[ROUTE.onActivate]);
    }

    const canActivate = (router) => (toState, fromState, done) => {
      const canActivateHandlers = handlers.map(handler => (router) => handler(toState, fromState, done));
      const canActivatePipedPromises = pipePromises(canActivateHandlers);

      return canActivatePipedPromises(router);
    };

    return {...route, canActivate};
  }
};
