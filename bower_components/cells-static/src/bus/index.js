import { Channel, AutoDisposableChannel } from './channel';
import { callbackHandler, registerHandler } from './proxyHelpers';


export default class Bus {
  __channels = {};

  channel(name) {
    if (!this.__channels[name]) {
      const channel = new AutoDisposableChannel(name);
      const disposeHandler = callbackHandler(this.__dispose, this);
      const proxiedChannel = registerHandler('dispose', channel, disposeHandler);
      this.__channels[name] = proxiedChannel;
    }

    return this.__channels[name];
  }

  broadcast(message) {
    Object.values(this.__channels).forEach(channel => channel.publish(message));
  }

  dispose() {
    Object.values(this.__channels).forEach(channel => channel.dispose());
    this.__channels = {};
  }

  __dispose(channel) {
    this.__channels[channel.name] = null;
    delete this.__channels[channel.name];
  }

}
